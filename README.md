adman
=====
ADMan is a tool for performing automated Active Directory management.

For more information, [Read the Docs](https://adman.readthedocs.io/).
