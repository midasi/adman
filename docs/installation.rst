Installation
============

Requirements
------------

The following Python packages are required:

- `setuptools <https://pypi.org/project/setuptools/>`_ -- For installation
- `pip <https://pypi.org/project/pip/>`_ -- For installation using pip (recommended)
- `python-ldap <https://www.python-ldap.org>`_ -- LDAP client
- `dnspython <https://www.dnspython.org>`_ -- DNS client
- `PyYAML <https://pyyaml.org>`_ -- YAML parser
- `pysmbc <https://github.com/hamano/pysmbc>`_ -- SMB client

  - Only required if ``userdirs`` configuration is present and ``user mkdirs`` or
    ``allmaint`` command is run

Also, your system must have the GSSAPI module for SASL authentication.

Where possible, it is preferable to install Python packages using your Linux
distribution's package manager, rather than from PyPI (using pip). This helps
avoid package conflicts.

Debian
~~~~~~
To install prerequisites on Debian::

    apt install \
        python3-setuptools \
        python3-pip \
        python3-ldap \
        python3-dnspython \
        python3-smbc \
        python3-yaml \
        libsasl2-modules-gssapi-mit

Fedora
~~~~~~
To install prerequisites on Fedora::

    dnf install \
        python3-setuptools \
        python3-pip \
        python3-ldap \
        python3-dns \
        python3-smbc \
        python3-pyyaml \
        cyrus-sasl-gssapi


Installation
------------

Then install Adman, either using pip::

    pip3 install adman

or from source::

    tar xf adman-*.tar.gz
    cd adman-*
    python3 setup.py install

