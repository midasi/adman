Find stale user / computer accounts
===================================

ADMan can find stale user and computer accounts in AD, send an email to an
admin, and optionally disable the accounts. The definition of "stale" is
configurable.

.. note::
   The `lastLogonTimestamp`_ LDAP attribute used to determine the staleness of
   an account is only updated every `msDS-LogonTimeSyncInterval`_ days, which
   defaults to 14. Therefore, the granularity cannot typically be set lower
   than this.

.. _lastLogonTimestamp: https://docs.microsoft.com/en-us/windows/win32/adschema/a-lastlogontimestamp
.. _msDS-LogonTimeSyncInterval: https://docs.microsoft.com/en-us/windows/win32/adschema/a-msds-logontimesyncinterval

Actions
-------------------------------------------------

For each configured user and computer LDAP container (or all, if none configured), ADMan will:

- Find all "stale" accounts, as configured either domain-wide or for that container
- Disable any stale accounts, if configured either domain-wide or for that container

Then ADMan will:

- Send an email to the admin (if ``email_to`` is set) with the findings and
  results, in tabular format.


Configuration
-------------------------------------------------

The following configuration options (keys) exist under ``stale_accounts``:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``email_to``
     - *string*
     - *(none)*
     - Email address to which reports are sent
   * - ``older_than``
     - :ref:`duration<config_type_duration>`
     - *(none)*
     - Minimum age of a "stale" account (domain default)
   * - ``disable``
     - *boolean*
     - False
     - Whether or not to disable stale accounts (domain default)
   * - ``include_disabled``
     - *boolean*
     - False
     - Whether or not to include disabled accounts in report (domain default)
   * - ``users``
     - :ref:`stale_accts<config_type_stale_accts>`
     - ``'all'``
     - User containers to be searched, along with settings which override domain defaults
   * - ``computers``
     - :ref:`stale_accts<config_type_stale_accts>`
     - ``'all'``
     - Computer containers to be searched, along with settings which override domain defaults

.. note::
   By default, the entire domain is searched for stale user and computer
   accounts. That can be overridden for user and computer accounts separately.
   LDAP containers to be searched can be specified here, along with settings
   which override those above.


.. _config_type_stale_accts:

*stale_accts* -- Like :ref:`containers<config_type_containers>` but with
additional keys, which override the domain defaults above:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``older_than``
     - :ref:`duration<config_type_duration>`
     - *(domain default)*
     - Minimum age of a "stale" account (override)
   * - ``disable``
     - *boolean*
     - *(domain default)*
     - Whether or not to disable stale accounts (override)
   * - ``include_disabled``
     - *boolean*
     - *(domain default)*
     - Whether or not to include disabled accounts in report (domain default)


Example configuration
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_config.yml
   :language: yaml
   :start-at: stale_accounts
   :end-before: end stale_accounts


Commands
-------------------------------------------------
Relevant CLI commands:

- :ref:`command_findstale`
