Tasks
=====

.. toctree::
   :maxdepth: 1
   :hidden:

   id-assign
   upn-suffix
   userdir
   passwd-exp-notify
   find-stale

ADMan supports the following tasks:

- :doc:`id-assign`: Assign :rfc:`2307` ``uidNumber``/``gidNumber`` attributes for users, computers and groups
- :doc:`upn-suffix`: Ensure UPN suffixes are consistent within an OU or across the domain (e.g., ensure every user's UPN matches his/her email address)
- :doc:`userdir`: Create per-user (e.g. "home") directories on multiple file servers
- :doc:`passwd-exp-notify`: Email users about expiring passwords
- :doc:`find-stale`: Find and disable stale accounts, and email a report to an admin
