UPN suffix consistency
======================

It is recommended that an AD domain be a subdomain of an organization's
top-level DNS domain name (e.g., ``ad.contoso.com``). It is also recommended
that each user's *user principal name* (UPN) match their email address (e.g.,
``jsmith@contoso.com``).

Together, these recommendations lead to the need to add a secondary UPN suffix:
one for the top-level domain. ADMan can ensure that users' UPNs are consistently
set.

References:

- `Microsoft Docs: User Naming Attributes: userPrincipalName <https://docs.microsoft.com/en-us/windows/win32/ad/naming-properties#userprincipalname>`_
- `Samba Wiki: Active Directory Naming FAQ <https://wiki.samba.org/index.php/Active_Directory_Naming_FAQ#My_User_Logins_Does_Not_Match_My_Email>`_

Actions
-------------------------------------------------
For each configured container, ADMan will enumerate the users and change their
``userPrincipalName``, if necessary, to match the desired UPN suffix.


Configuration
-------------------------------------------------
``upn_suffixes`` is a *mapping* (dictionary) similar to the
:ref:`containers<config_type_containers>` type, where the *key* is the the
container holding the users to which the UPN suffix will be applied. The
*value* is either 1. the UPN suffix to apply, or 2. a mapping with the
following keys:

.. list-table::
   :widths: 20 20 20 40
   :header-rows: 1

   * - Config Key
     - Type
     - Default
     - Description
   * - ``suffix``
     - *string*
     - *(required)*
     - The UPN suffix to apply
   * - ``scope``
     - *string*
     - ``subtree``
     - Scope of LDAP search in the container: either ``one`` or ``subtree``

Example configuration
~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../example_config.yml
   :language: yaml
   :start-at: upn_suffixes
   :end-before: end upn_suffixes


Commands
-------------------------------------------------
Relevant CLI commands:

- :ref:`command_user_setupns`
