Troubleshooting
===============

No worthy mechs found
---------------------
::

    ldap.AUTH_UNKNOWN: {'desc': 'Unknown authentication method', 'errno': 22, 'info': 'SASL(-4): no mechanism available: No worthy mechs found'}

You need to install the GSSAPI SASL modules. On Debian::

    apt install libsasl2-modules-gssapi-mit


Insufficient access
-------------------
::

    ldap.INSUFFICIENT_ACCESS: {'desc': 'Insufficient access', 'info': '00002098: Object CN=adtest,CN=ypservers,CN=ypServ30,CN=RpcServices,CN=System,DC=ad-test,DC=vx has no write property access\n'}

The ADMan user needs to be a member of ``Domain Admins``.

Once this change has been made, you must remove the stale credential cache, e.g.::

    rm /tmp/domain-janitor.cc


Server not found in Kerberos database
-------------------------------------
::

    SASL: GSSAPI Error: Unspecified GSS failure. Minor code may provide more information (Server not found in Kerberos database).

Various problems can lead to this error. One common case I've encountered is
that a reverse DNS (PTR) record does not exist for the DC(s).
