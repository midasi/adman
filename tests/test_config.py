import pytest
from datetime import timedelta
from pathlib import Path
from textwrap import dedent as D
from textwrap import dedent
from functools import wraps
import yaml

import adman.config
from adman.config import Config, ConfigError, DurationConfig

tests_dir = Path(__file__).parent
proj_root = tests_dir.parent


def assertPathEqual(p1, p2):
    __tracebackhide__ = True
    assert Path(p1) == Path(p2)


def config_from_text(txt):
    boiler = D("""\
              domain: example.com
              ldap_auth:
                mode: gssapi
              """)
    txt = boiler + D(txt)
    data = yaml.safe_load(txt)
    cfg = Config(data, __file__)

    print("Test config:")
    print(cfg)

    return cfg

def assert_is_default_all(contcfg):
    assert len(contcfg) == 1
    assert contcfg[None].scope == 'subtree'

class Test_ProjectExampleConfig:
    @pytest.fixture
    def cfg(self):
        return Config.load(proj_root / "example_config.yml")

    def test_toplevel(self, cfg):
        assert cfg.domain == "ad.example.com"
        assertPathEqual(cfg.changelog_path, "/var/log/adman-changes.log")

    def test_ldap_auth(self, cfg):
        la = cfg.ldap_auth
        assert la.mode == "gssapi"
        assert la.username == "domain-janitor"

        # Test relative path
        assert la.keytab == proj_root / "domain-janitor.keytab"

        # Test absolute path
        assertPathEqual(la.cache, "/tmp/domain-janitor.cc")

    def test_id_assign(self, cfg):
        ia = cfg.id_assign
        assert ia is not None
        assert ia.uid_range == range(100000, 200000)
        assert ia.gid_range == range(100000, 200000)

        assert len(ia.only) == 4

        o = ia.only['CN=Users']
        assert o.scope == "subtree"

        o = ia.only['CN=Computers']
        assert o.scope == "subtree"

        o = ia.only['OU=Domain Controllers']
        assert o.scope == "subtree"

        o = ia.only['OU=ADTest People']
        assert o.scope == "one"

    def test_upn_suffixes(self, cfg):
        assert len(cfg.upn_suffixes) == 2

        u = cfg.upn_suffixes['CN=Users']
        assert u.suffix == "example.com"
        assert u.scope == "subtree"

        u = cfg.upn_suffixes['OU=Special Users,OU=People']
        assert u.suffix == "special.com"
        assert u.scope == "one"

    def test_passwod_expiry(self, cfg):
        assert cfg.pwexp.days == [7, 3, 2, 1, 0]
        with open(proj_root / "example_pwnotify.tmpl") as f:
            tmpl = f.read()
        assert cfg.pwexp.template == tmpl

    def test_smtp(self, cfg):
        assert cfg.smtp.email_from == "Domain Janitor <domain-janitor@example.com>"
        assert cfg.smtp.host == "smtp.example.com"
        assert cfg.smtp.port == 25
        assert cfg.smtp.username == "joe"
        assert cfg.smtp.password == "password"
        assert cfg.smtp.encryption == "starttls"

    def test_stale_accounts(self, cfg):
        sa = cfg.stale_accounts

        default_older_than = timedelta(days=120)

        assert sa.email_to == "System Administrator <sysadmin@example.com>"
        assert sa.older_than.dt == default_older_than
        assert sa.disable == False     # default
        assert sa.include_disabled == True

        # users
        assert len(sa.users) == 2

        c = sa.users['OU=Special Users,OU=People']
        assert c.scope == 'one'
        assert c.older_than.dt == timedelta(days=30)
        assert c.disable == True
        assert c.include_disabled == False

        c = sa.users['CN=Users']
        assert c.scope == 'subtree'
        assert c.older_than.dt == default_older_than
        assert c.disable == False
        assert c.include_disabled == True

        # computers
        assert len(sa.computers) == 2

        c = sa.computers['CN=Computers']
        assert c.scope == 'subtree'
        assert c.older_than.dt == default_older_than
        assert c.disable == True
        assert c.include_disabled == True

        c = sa.computers['OU=Domain Controllers']
        assert c.scope == 'subtree'
        assert c.older_than.dt == default_older_than
        assert c.disable == False
        assert c.include_disabled == True

        # TODO: Test the corner cases:
        # - empty users/computers
        # - various missing/defaulted attributes


class Test_Config:
    @staticmethod
    def get_dummy_cfgdata(**kw):
        """Get minimal dummy data for loading config"""
        data = dict(
            domain = "ad-test.vx",
            id_assign = dict(
                uid_range = dict(min=100, max=200),
                gid_range = dict(min=100, max=200),
            ),
            ldap_auth = dict(
                mode = "gssapi",
                krb_username = "user",
                krb_keytab = "user.keytab",
                krb_cache = "/tmp/user.cc",
            ),
        )
        data.update(kw)
        return data


    def test_keynames(self):
        cfg = Config(self.get_dummy_cfgdata(), __file__)
        assert cfg.keyname == ""
        assert cfg.ldap_auth.keyname == "ldap_auth"
        assert cfg.ldap_auth.subkeyname("mode") == "ldap_auth.mode"

    def test_only_scope(self):
        data = self.get_dummy_cfgdata()
        data['id_assign']['only'] = {
            'OU=One':   None,
            'OU=Two':   dict(scope = 'subtree'),
            'OU=Three': dict(scope = 'one'),
        }
        cfg = Config(data, __file__)
        ia = cfg.id_assign

        assert len(ia.only) == 3

        o = ia.only['OU=One']
        assert o.scope == 'subtree'

        o = ia.only['OU=Two']
        assert o.scope == 'subtree'

        o = ia.only['OU=Three']
        assert o.scope == 'one'

        assert len(ia.only) == 3
        assert ia.only['OU=One'].scope == 'subtree'
        assert ia.only['OU=Two'].scope == 'subtree'
        assert ia.only['OU=Three'].scope == 'one'

    def test_no_only(self):
        cfg = Config(self.get_dummy_cfgdata(), __file__)
        assert len(cfg.id_assign.only) == 1
        assert cfg.id_assign.only[None].scope == 'subtree'


    def test_missing_domain(self):
        data = self.get_dummy_cfgdata()
        del data['domain']

        with pytest.raises(ConfigError, match="domain"):
            cfg = Config(data, __file__)


    def test_missing_smtp(self):
        """Ensure SMTP settings can be missing"""
        cfg = Config(self.get_dummy_cfgdata(), __file__)
        assert cfg.smtp is None

    def test_missing_smtp_with_pwexp(self):
        """Ensure SMTP settings required if password expiry used"""
        data = self.get_dummy_cfgdata(
            password_expiry_notification = dict(
                days = 7,
                template_file = '/dev/null',
            ),
        )

        with pytest.raises(ConfigError, match="smtp"):
            Config(data, __file__)


    def test_id_assign_no_only(self):
        cfg = config_from_text(
                """\
                id_assign:
                  uid_range: {min: 1, max: 2}
                  gid_range: {min: 1, max: 2}
                """)
        assert_is_default_all(cfg.id_assign.only)

    def test_id_assign_only_empty(self):
        cfg = config_from_text(
                """\
                id_assign:
                  uid_range: {min: 1, max: 2}
                  gid_range: {min: 1, max: 2}
                  only:
                """)
        assert len(cfg.id_assign.only) == 0

    def test_id_assign_only_all(self):
        cfg = config_from_text(
                """\
                id_assign:
                  uid_range: {min: 1, max: 2}
                  gid_range: {min: 1, max: 2}
                  only: 'all'
                """)
        assert_is_default_all(cfg.id_assign.only)

    def test_container_config_absent_or_default(self):
        cfg = config_from_text(
                  """\
                  stale_accounts:
                    older_than: 120 days
                    # users is absent
                    computers: all
                  """)


        assert_is_default_all(cfg.stale_accounts.users)
        assert_is_default_all(cfg.stale_accounts.computers)


    def test_container_config_empty_or_null(self):
        cfg = config_from_text(
                  # Empty key in yaml is same as 'null'
                  # but we treat it as "empty" (same as {})
                  """\
                  stale_accounts:
                    older_than: 120 days
                    users: null
                    computers: {}
                  """)

        assert len(cfg.stale_accounts.users) == 0
        assert len(cfg.stale_accounts.computers) == 0


    def tests_upn_suffixes_all_not_allowed(self):
        """Verify that 'upn_suffixes: all' is forbidden

        This config set requires explicit entries. If it is not set, the
        feature is disabled.
        """
        with pytest.raises(ConfigError, match="'all' not allowed"):
            cfg = config_from_text(
                      """\
                      upn_suffixes: all
                      """)



class Test_DurationConfig:
    @staticmethod
    def _get_durcfg(humanstr):
        return DurationConfig(humanstr, "dummy", None)

    def _t(self, humanstr, **dtkw):
        dc = self._get_durcfg(humanstr)
        dt = timedelta(**dtkw)
        assert dc.dt == dt
        return dc

    def test_duration_singular_second(self):
        self._t("1 second", seconds=1)

    def test_duration_multi_seconds(self):
        self._t("42 seconds", seconds=42)

    def test_duration_singular_minute(self):
        self._t("1 minute", minutes=1)

    def test_duration_multi_minutes(self):
        self._t("9 minutes", minutes=9)

    def test_duration_singular_hour(self):
        self._t("1 hour", hours=1)

    def test_duration_multi_hours(self):
        self._t("13 hours", hours=13)

    def test_duration_singular_day(self):
        self._t("1 day", days=1)

    def test_duration_multi_days(self):
        self._t("7 days", days=7)

    def test_duration_singular_week(self):
        self._t("1 week", weeks=1)

    def test_duration_multi_weeks(self):
        self._t("2 weeks", weeks=2)

    def test_duration_singular_year(self):
        self._t("1 year", days=365)

    def test_duration_multi_years(self):
        self._t("4 years", days=4*365)

    def test_str(self):
        """str() return original input string"""
        humanstr = "123   DaYs"
        dc = self._t(humanstr, days=123)
        assert str(dc) == humanstr
