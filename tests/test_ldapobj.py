import pytest
from unittest.mock import MagicMock
from adman.ldapobj import LdapObject, IntAttr, StrAttr
import ldap

class DummyObj(LdapObject):
    _known_attrs = (
        IntAttr('int_ro'),
        IntAttr('int_rw', writable=True),
        IntAttr('int_atom', writable=True, atomic=True),
        StrAttr('str_ro'),
        StrAttr('pyname', ldapattr='ldapAttrName'),
    )

    delta = 1000

    @property
    def xformed(self):
        return self.int_rw + self.delta

    @xformed.setter
    def xformed(self, value):
        self.int_rw = value - self.delta


class DummyADManager:
    def __init__(self):
        self._modify = MagicMock()


class TestLdapObject:
    @pytest.fixture(autouse=True)
    def _setup(self):
        self.ad = DummyADManager()


    def _get_dummy_obj(self, **kw):
        return DummyObj(ad=self.ad, dn='CN=dontcare,DC=example,DC=com', **kw)


    def test_construct(self):
        """Assert that an object can be constructed from ldap data"""
        o = self._get_dummy_obj(
                    int_ro = [b'1'],
                    int_rw = [b'2'],
                    str_ro = [b'hello'],
                    ldapAttrName = [b'goodbye']
                )

        assert o.int_ro == 1
        assert o.int_rw == 2
        assert o.str_ro == 'hello'
        assert o.pyname == 'goodbye'


    def test_construct_missing(self):
        """Verify construction of an object with missing LDAP attributes data works"""
        # Allow the object to be created with missing (unnecessary) LDAP attr
        # values.
        o = self._get_dummy_obj()

        # Missing LDAP attrs result in Python attrs set to None.
        # TODO: Ideally, if a user omitted the attributes from search(), then
        # we would omit them here. But we can't currently distinguish that from
        # attributes which *were* requested, but were simply not set in the
        # LDAP database. Perhaps we can change the semantics of __init__ to
        # distinguish these two cases.
        assert o.int_ro == None


    def test_readonly(self):
        """Assert that trying to modify a read-only attribute raises an exception"""
        o = self._get_dummy_obj(
                    int_ro = [b'1'],
                )
        with pytest.raises(AttributeError, match="read-only"):
            o.int_ro = 666


    def test_set_unknown_attr(self):
        """Assert that trying to set an unknown attribute raises an exception"""
        o = self._get_dummy_obj()
        with pytest.raises(AttributeError, match="does not allow attribute 'some_made_up_attr' to be set"):
            o.some_made_up_attr = 666



    def _test_modify_one(self, o):
        # Change value
        o.int_rw = 4

        # Ensure it sticks
        assert o.int_rw == 4

        o.commit()

        self.ad._modify.assert_called_once_with(o.dn, [
                # Non-atomic: replace whatever is there (maybe nothing)
                (ldap.MOD_REPLACE, 'int_rw', [b'4']),
            ])


    def test_modify_one(self):
        """Verify that modifying one attribute works"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )
        self._test_modify_one(o)


    def test_modify_missing(self):
        """Verify that setting a missing attribute works"""
        o = self._get_dummy_obj()
        self._test_modify_one(o)


    def test_modify_wrong_type(self):
        """Setting the wrong type raises an exception"""
        o = self._get_dummy_obj()
        with pytest.raises(TypeError):
            o.int_rw = 'foo'


    def test_modify_None(self):
        """Verify a value can be set to None"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )

        # Change value
        o.int_rw = None

        # Ensure it sticks
        assert o.int_rw == None

        o.commit()

        self.ad._modify.assert_called_once_with(o.dn, [
                # Non-atomic: replace whatever is there (maybe nothing)
                (ldap.MOD_REPLACE, 'int_rw', []),
            ])


    def test_modify_atomic(self):
        """Verify that modifying an atomic attribute works"""
        o = self._get_dummy_obj(
                    int_atom = [b'3'],
                )
        o.int_atom = 6
        o.commit()

        self.ad._modify.assert_called_once_with(o.dn, [
                # Atomic: delete old value
                # https://ldapwiki.com/wiki/LDIF%20Atomic%20Operations
                (ldap.MOD_DELETE, 'int_atom', [b'3']),
                (ldap.MOD_ADD,    'int_atom', [b'6']),
            ])


    def test_set_missing_atomic(self):
        o = self._get_dummy_obj()

        o.int_atom = 6
        o.commit()

        self.ad._modify.assert_called_once_with(o.dn, [
                # Atomic: only 'ADD' (can't delete if not present)
                # Using ADD ensures that if it is already set (but we don't
                # know), then it will fail.
                (ldap.MOD_ADD,    'int_atom', [b'6']),
            ])


    def test_clear_atomic(self):
        o = self._get_dummy_obj(
                    int_atom = [b'3'],
                )

        o.int_atom = None
        o.commit()

        self.ad._modify.assert_called_once_with(o.dn, [
                # Atomic: only 'DELETE'
                (ldap.MOD_DELETE, 'int_atom', [b'3']),
            ])


    def test_modify_multiple_times_same_value(self):
        """Verify that setting an attribute multiple times to the same value results in one change"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )

        # Make a series of changes
        for i in range(5):
            o.int_rw = 3
        assert o.int_rw == 3

        o.commit()

        # One change should be written
        self.ad._modify.assert_called_once_with(o.dn, [
                # Non-atomic: replace whatever is there (maybe nothing)
                (ldap.MOD_REPLACE, 'int_rw', [b'3']),
            ])


    def test_modify_multiple_times_different_values(self):
        """Verify that setting an attribute multiple times results in one change"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )

        # Make a series of changes
        for i in range(2, 7+1):
            o.int_rw = i
        assert o.int_rw == 7

        o.commit()

        # Only the last should be written
        self.ad._modify.assert_called_once_with(o.dn, [
                # Non-atomic: replace whatever is there (maybe nothing)
                (ldap.MOD_REPLACE, 'int_rw', [b'7']),
            ])


    def test_modify_noop(self):
        """Verify that setting an attribute to its same value results in no change"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )

        # Set the same value
        o.int_rw = 2
        assert o.int_rw == 2

        o.commit()

        # Assert no changes
        self.ad._modify.assert_not_called()


    def test_modify_noop2(self):
        """Verify that restoring an attribute to its original value results in no change"""
        o = self._get_dummy_obj(
                    int_rw = [b'2'],
                )

        # Change it
        o.int_rw = 3
        assert o.int_rw == 3

        # Put it back
        o.int_rw = 2
        assert o.int_rw == 2

        o.commit()

        # Assert no changes
        self.ad._modify.assert_not_called()


    def test_property_get(self):
        """Verify that getter properties work on LdapObject children"""
        o = self._get_dummy_obj(
                    int_rw = [b'7'],
                )

        assert o.xformed == 1007


    def test_property_set(self):
        """Verify that setter properties work on LdapObject children"""
        o = self._get_dummy_obj(
                    int_rw = [b'7'],
                )

        o.xformed = 1009
        assert o.int_rw == 9
