from adman.types import *

class TestSID:
    rawsid = b'\x01\x05\x00\x00\x00\x00\x00\x05\x15\x00\x00\x00\x69\x35' \
             b'\xb7\x71\x98\xd9\xdd\xdd\xbc\x55\x10\xda\x50\x04\x00\x00'
    sidstr = 'S-1-5-21-1907832169-3722303896-3658503612-1104'
    rid = 1104

    def test_from_bytes(self):
        sid = SID.from_bytes(self.rawsid)
        assert str(sid) == self.sidstr
        assert sid.rid == self.rid

    def test_from_str(self):
        sid = SID.from_str(self.sidstr)
        assert str(sid) == self.sidstr
        assert sid.rid == self.rid

    def test_to_bytes(self):
        sid = SID.from_str(self.sidstr)
        assert sid.to_bytes() == self.rawsid

    def test_get_set_rid(self):
        sid = SID.from_str(self.sidstr)
        sid.rid = 1234
        exp_sidstr = '-'.join(self.sidstr.split('-')[:-1] + ['1234'])
        assert str(sid) == exp_sidstr
