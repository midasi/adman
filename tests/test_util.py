import pytest
from datetime import datetime, timezone
from adman.util import *

class Test_count:
    def test_count_empty(self):
        assert count([]) == 0

    def test_count_empty_gen(self):
        def gen():
            return
            yield
        assert count(gen()) == 0

    def test_count_list(self):
        assert count([1, 2, 3]) == 3

    def test_count_gen(self):
        def gen():
            yield 1
            yield 2
            yield 3
        assert count(gen()) == 3

    def test_count_list_cond(self):
        assert count([1, 2, 3, 4], lambda x: x % 2) == 2

    def test_count_list_cond_nomatch(self):
        assert count([1, 2, 3, 4], lambda x: x > 100) == 0


class Test_single:
    def test_single_one(self):
        seq = [1]
        assert single(seq) == 1

    def test_single_one_match(self):
        seq = [1, 200]
        assert single(seq, lambda x: x > 10) == 200

    def test_single_empty(self):
        seq = []
        with pytest.raises(IndexError):
            single(seq)

    def test_single_none_match(self):
        seq = [1, 2, 3]
        with pytest.raises(IndexError):
            single(seq, lambda x: x > 10)

    def test_multiple(self):
        seq = [1, 2, 3]
        with pytest.raises(IndexError):
            single(seq)

    def test_multiple_match(self):
        seq = [10, 20, 30]
        with pytest.raises(IndexError):
            single(seq, lambda x: x > 10)

    def test_single_one_None(self):
        seq = [None]
        assert single(seq) == None


    def test_single_or(self):
        seq = []
        assert single_or(seq, 42) == 42



class Test_int_from_bytes:
    def test_simple_big(self):
        data = b'\x01\x02\x03\x04'
        val = int_from_bytes(data, 0, 4, 'big')
        assert 0x01020304 == val

    def test_simple_little(self):
        data = b'\x01\x02\x03\x04'
        val = int_from_bytes(data, 0, 4, 'little')
        assert 0x04030201 == val

    def test_offset(self):
        data = b'\xFF\xFF\x01\x02\x03\x04'
        val = int_from_bytes(data, 2, 4, 'little')
        assert 0x04030201 == val

    def test_extra_length(self):
        data = b'\xFF\xFF\x01\x02\x03\x04\xFF\xFF\xFF\xFF\xFF'
        val = int_from_bytes(data, 2, 4, 'little')
        assert 0x04030201 == val

    def test_raise_on_short(self):
        data = b'\x01\x02\x03'
        with pytest.raises(ValueError):
            int_from_bytes(data, 0, 4, 'little')


class Test_join_nonempty:
    def test_no_args(self):
        assert join_nonempty('!') == ''

    def test_all_empty_args(self):
        assert join_nonempty('!', '') == ''
        assert join_nonempty('!', '', '') == ''
        assert join_nonempty('!', '', '', '') == ''

    def test_one_empty_arg(self):
        assert join_nonempty('!', '', 'a', 'b') == 'a!b'
        assert join_nonempty('!', 'a', '', 'b') == 'a!b'
        assert join_nonempty('!', 'a', 'b', '') == 'a!b'

    def test_no_empty_args(self):
        assert join_nonempty('!', 'a', 'b') == 'a!b'
        assert join_nonempty('!', 'a', 'b', 'c') == 'a!b!c'
        assert join_nonempty('!', 'abc', 'def') == 'abc!def'


class Test_FILETIME:
    # Friday, January 17, 2020 6:28:11pm
    SOME_FILETIME = 132237592910000000
    SOME_DATETIME = datetime(year=2020, month=1, day=17, hour=18, minute=28, second=11, tzinfo=timezone.utc)

    MAX_FILETIME = INT64_MAX

    def assertIsInUTC(self, dt):
        __tracebackhide__ = True
        assert dt.tzinfo == timezone.utc

    def test__FILETIME_to_datetime__negative(self):
        with pytest.raises(ValueError):
            FILETIME_to_datetime(-1234)

    def test__FILETIME_to_datetime__normal(self):
        dt = FILETIME_to_datetime(self.SOME_FILETIME)
        assert dt == self.SOME_DATETIME
        self.assertIsInUTC(dt)

    def test__FILETIME_to_datetime__overflow(self):
        dt = FILETIME_to_datetime(self.MAX_FILETIME)
        assert dt == UTCMAX
        self.assertIsInUTC(dt)

    def test__datetime_to_FILETIME__normal(self):
        ft = datetime_to_FILETIME(self.SOME_DATETIME)
        assert ft == self.SOME_FILETIME

    def test__datetime_to_FILETIME__max(self):
        ft = datetime_to_FILETIME(UTCMAX)
        assert ft == self.MAX_FILETIME

    def test__datetime_to_FILETIME__requires_offset_aware(self):
        with pytest.raises(TypeError, match=r"can't \w* offset-naive and offset-aware datetimes"):
            datetime_to_FILETIME(datetime.now())
